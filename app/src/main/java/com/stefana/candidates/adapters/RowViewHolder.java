package com.stefana.candidates.adapters;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.stefana.candidates.R;
import com.stefana.candidates.dtos.Kandidat;
import com.stefana.candidates.views.FormActivity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RowViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.imeIprezime)
    public TextView imeIPrezime;
    @BindView(R.id.jmbg)
    public TextView jmbg;
    @BindView(R.id.konkurs)
    public TextView konkurs;
    @BindView(R.id.tel)
    public TextView telefon;
    @BindView(R.id.mail)
    public TextView email;
    public Kandidat kandidat;

    public RowViewHolder(@NonNull final View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);


        itemView.setOnClickListener(v1 -> {
            Intent intent = new Intent(itemView.getContext(), FormActivity.class);
            intent.putExtra(FormActivity.ID, kandidat.getId());
            itemView.getContext().startActivity(intent);
        });


    }
}
