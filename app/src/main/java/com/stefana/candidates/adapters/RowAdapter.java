package com.stefana.candidates.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.stefana.candidates.R;
import com.stefana.candidates.dtos.Kandidat;
import com.stefana.candidates.views.FormActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RowAdapter extends RecyclerView.Adapter<RowViewHolder> {

    List<Kandidat> kandidati = new ArrayList<>();


    public RowAdapter() {
    }

    @NonNull
    @Override
    public RowViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();

        View v = LayoutInflater
                .from(context)
                .inflate(R.layout.candidate_view_holder, viewGroup, false);


        return new RowViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RowViewHolder holder, int position) {
        Kandidat kandidat = kandidati.get(position);
        holder.kandidat = kandidat;
        holder.imeIPrezime.setText(kandidat.getPrezime() + " " + kandidat.getIme());
        holder.jmbg.setText("JMBG: " + kandidat.getJmbg());
        holder.konkurs.setText("Concurs: " + kandidat.getKonkurs().getImeKonkursa());
        holder.telefon.setText("Cellphone: " + kandidat.getTelefon());
        holder.email.setText("Email: " + kandidat.getEmail());
    }

    @Override
    public int getItemCount() {

            return kandidati.size();
    }

    public void setItems(ArrayList<Kandidat> kandidati) {
        this.kandidati = kandidati;

    }


    public List<Kandidat> getItems() {
        return kandidati;
    }



}
