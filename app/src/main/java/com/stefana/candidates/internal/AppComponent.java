package com.stefana.candidates.internal;

import com.stefana.candidates.views.FormActivity;
import com.stefana.candidates.views.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton @Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(CandidatesApplication candidatesApplication);
    void inject(MainActivity mainActivity);
    void inject(FormActivity formActivity);
}
