package com.stefana.candidates.internal;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.stefana.candidates.dtos.Konkurs;
import com.stefana.candidates.internal.DaggerAppComponent;


import io.realm.Realm;

public class CandidatesApplication extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        Stetho.initializeWithDefaults(this);

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        appComponent.inject(this);

        ubudziJedanKonkursUPraznuBazu();
    }

    private void ubudziJedanKonkursUPraznuBazu() {
        try {
            Realm.getDefaultInstance().beginTransaction();
            Realm.getDefaultInstance().copyToRealm(new Konkurs(2, "Glassdoor"));
            Realm.getDefaultInstance().copyToRealm(new Konkurs(3, "Startit"));
            Realm.getDefaultInstance().copyToRealm(new Konkurs(4, "Indeed"));
            Realm.getDefaultInstance().copyToRealm(new Konkurs(1, "Infostud"));

            Realm.getDefaultInstance().commitTransaction();
        } catch (Exception e) {

            Realm.getDefaultInstance().commitTransaction();
        }
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
