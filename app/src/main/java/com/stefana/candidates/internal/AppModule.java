package com.stefana.candidates.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.stefana.candidates.presenters.FormPresenter;
import com.stefana.candidates.presenters.MainPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;


@Module
public class AppModule {

    private final CandidatesApplication candidatesApplication;
    private Realm realmDatabse;

    public AppModule(CandidatesApplication candidatesApplication) {
        this.candidatesApplication = candidatesApplication;
    }

    @Provides
    @Singleton
    MainPresenter provideMainPresenter(Realm realm) {
        return new MainPresenter(realm);
    }

    @Provides
    @Singleton
    FormPresenter provideFormPresenter(Realm realm) {
        return new FormPresenter(realm);
    }

    @Provides
    @Singleton
    Context providesAppContext() {
        return candidatesApplication;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPref(Context context) {
        return context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
    }

    @Provides
    Resources getResources(Context context) {
        return context.getResources();
    }

    @Provides
    @Singleton
    Realm provideRealm() {
        if (realmDatabse == null) {
            realmDatabse = Realm.getDefaultInstance();
        }
        return realmDatabse;
    }
}
