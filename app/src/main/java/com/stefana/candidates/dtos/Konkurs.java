package com.stefana.candidates.dtos;

import java.util.Objects;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Konkurs extends RealmObject {

    @PrimaryKey
    private int id;
    private String imeKonkursa;

    public Konkurs(int id, String imeKonkursa) {
        this.id = id;
        this.imeKonkursa = imeKonkursa;
    }

    public Konkurs() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImeKonkursa() {
        return imeKonkursa;
    }

    public void setImeKonkursa(String imeKonkursa) {
        this.imeKonkursa = imeKonkursa;
    }

    @Override
    public String toString() {
        return imeKonkursa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Konkurs konkurs = (Konkurs) o;

        if (id != konkurs.id) return false;
        return imeKonkursa != null ? imeKonkursa.equals(konkurs.imeKonkursa) : konkurs.imeKonkursa == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (imeKonkursa != null ? imeKonkursa.hashCode() : 0);
        return result;
    }
}
