package com.stefana.candidates.dtos;
import java.util.Objects;

import androidx.annotation.NonNull;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Kandidat extends RealmObject {

    @PrimaryKey
    private long id = -1;
    private String ime;
    private String prezime;
    private String jmbg;
    private int godinaRodjenja;
    private String email;
    private String telefon;
    private Konkurs konkurs;
    private String napomena;
    private Boolean daLiJeZaposlen;
    private long datumIzmene;

    public Kandidat() {
    }

    public Kandidat(String ime, String prezime, String jmbg,
                    int godinaRodjenja, String email,
                    String telefon, Konkurs konkurs,
                    String napomena, Boolean daLiJeZaposlen,
                    long datumIzmene) {
        this.ime = ime;
        this.prezime = prezime;
        this.jmbg = jmbg;
        this.godinaRodjenja = godinaRodjenja;
        this.email = email;
        this.telefon = telefon;
        this.konkurs = konkurs;
        this.napomena = napomena;
        this.daLiJeZaposlen = daLiJeZaposlen;
        this.datumIzmene = datumIzmene;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public int getGodinaRodjenja() {
        return godinaRodjenja;
    }

    public void setGodinaRodjenja(int godinaRodjenja) {
        this.godinaRodjenja = godinaRodjenja;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Konkurs getKonkurs() {
        return konkurs;
    }

    public void setKonkurs(Konkurs konkurs) {
        this.konkurs = konkurs;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public Boolean getDaLiJeZaposlen() {
        return daLiJeZaposlen;
    }

    public void setDaLiJeZaposlen(Boolean daLiJeZaposlen) {
        this.daLiJeZaposlen = daLiJeZaposlen;
    }

    public long getDatumIzmene() {
        return datumIzmene;
    }

    public void setDatumIzmene(long datumIzmene) {
        this.datumIzmene = datumIzmene;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Kandidat kandidat = (Kandidat) o;

        return id == kandidat.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @NonNull
    @Override
    public String toString() {
        return ime + " " + prezime + " " + jmbg;
    }
}
