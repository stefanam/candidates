package com.stefana.candidates.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Spinner;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.stefana.candidates.adapters.SwipeController;
import com.stefana.candidates.adapters.SwipeControllerActions;
import com.stefana.candidates.dtos.Konkurs;
import com.stefana.candidates.internal.CandidatesApplication;
import com.stefana.candidates.presenters.MainPresenter;
import com.stefana.candidates.R;
import com.stefana.candidates.adapters.RowAdapter;
import com.stefana.candidates.adapters.SpacesItemDecoration;
import com.stefana.candidates.dtos.Kandidat;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainPresenter.MainView {

    private RecyclerView recyclerView;
    private RowAdapter adapter;
    private SwipeController swipeController;
    private Spinner spinner;
    private BottomNavigationView bottomNavigationView;
    private SwipeRefreshLayout refreshLayout;

    @Inject
    SharedPreferences prefs;

    @Inject
    MainPresenter presenter;

    public MainActivity() {
    }

    //dagger initializes presenter

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        refreshLayout = findViewById(R.id.swipe);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadData();
                refreshLayout.setRefreshing(false);
            }
        });

        ((CandidatesApplication) getApplication()).getAppComponent().inject(this);

        //init recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView = findViewById(R.id.recycler_view_list_of_candidates);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new SpacesItemDecoration(12));
        recyclerView.setAdapter(adapter = new RowAdapter());


        //init fab
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, FormActivity.class);
            startActivity(intent);
        });

        //handle deleting candidate
        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(final int position) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.comfirm);
                builder.setMessage(R.string.are_you_sure);
                builder.setPositiveButton(R.string.yes, (dialogInterface, i) -> {
                    //deleting row
                    presenter.deleteData(adapter.getItems().get(position).getId());

                });
                builder.setNegativeButton(R.string.no, (dialogInterface, i) -> {
                });
                builder.show();
            }

        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeController);
        itemTouchHelper.attachToRecyclerView(recyclerView);



        spinner = findViewById(R.id.konkursi);

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.show_recent:

                        menuItem.setChecked(true);

                        Log.e("Stefana", "show recents button is clicked");
                        //sort by time of change

                        presenter.showRecentData();

                        menuItem.setChecked(false);

                        break;

                    case R.id.add_concurs:

                        menuItem.setChecked(false);

                        Log.e("Stefana", "add concurs button is clicked");


                        EditText editText = new EditText(MainActivity.this);
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
                        builder.setTitle(R.string.add_concurs);
                        builder.setMessage(R.string.enter_new_concurs);
                        builder.setView(editText);
                        builder.setPositiveButton(R.string.comfirm, (dialogInterface, i) -> {
                            //adding new concurs


                            presenter.insertConcurs(editText.getText().toString());

                        });
                        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                        });
                        builder.show();

                        menuItem.setChecked(false);

                        break;
                }

                return true;
            }
        });

    }


    @Override
    public void initKonkursSpiner(ArrayList<Konkurs> list) {

        ArrayAdapter<Konkurs> dataAdapter = new ArrayAdapter<Konkurs>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("Selected Item is --> ", list.get(position).getImeKonkursa());

                presenter.loadData();

                if (position == 0) {
                    presenter.loadData();
                } else {
                    presenter.loadData(list.get(position));
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                presenter.loadData();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume(this);   //connect with presenter
        presenter.loadKonkursData(); //ucitava dostupne konkurse iz baze
        presenter.loadData();            //load candidates
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void presentData(ArrayList<Kandidat> data) {
        //reload data
        adapter.setItems(data);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showError(String localizedMessage) {

        new AlertDialog.Builder(this)
                .setMessage(localizedMessage)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {

                })
                .setCancelable(true)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        MenuItem menuItem = menu.findItem(R.id.action_search);
        androidx.appcompat.widget.SearchView searchView = (androidx.appcompat.widget.SearchView) MenuItemCompat.getActionView(menuItem);

        searchView.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                presenter.filterData(newText, (Konkurs) spinner.getSelectedItem());
                //getFilter().filter(newText);
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
