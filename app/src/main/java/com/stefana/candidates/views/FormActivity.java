package com.stefana.candidates.views;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.stefana.candidates.R;
import com.stefana.candidates.dtos.Kandidat;
import com.stefana.candidates.dtos.Konkurs;
import com.stefana.candidates.internal.CandidatesApplication;
import com.stefana.candidates.presenters.FormPresenter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Pattern;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FormActivity extends AppCompatActivity implements FormPresenter.View {

    private static final SimpleDateFormat format = new SimpleDateFormat("dd.MMM.yyyy. HH:mm:ss");
    public static final String ID = "id";
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    @BindView(R.id.confirm)
    Button confirm;
    @BindView(R.id.ime_wrapper)
    TextInputLayout nameWrapper;
    @BindView(R.id.ime)
    EditText name;
    @BindView(R.id.prezime_wrapper)
    TextInputLayout prezimeWrapper;
    @BindView(R.id.prezime)
    EditText surname;
    @BindView(R.id.jmbg_wrapper)
    TextInputLayout jmbgWrapper;
    @BindView(R.id.jmbg)
    EditText jmbg;
    @BindView(R.id.godina_rodjenja_wrapper)
    TextInputLayout godinaRodjenjaWrapper;
    @BindView(R.id.godina_rodjenja)
    EditText yearOfBirth;
    @BindView(R.id.email_wrapper)
    TextInputLayout emailWrapper;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.telefon_wrapper)
    TextInputLayout cellphoneWrapper;
    @BindView(R.id.telefon)
    EditText cellphone;
    @BindView(R.id.konkurs)
    Spinner konkurs;
    @BindView(R.id.napomena_wrapper)
    TextInputLayout notesWrapper;
    @BindView(R.id.napomena)
    EditText notes;
    @BindView(R.id.da_li_je_zaposlen)
    CheckBox isHired;
    @BindView(R.id.datum_izmene)
    TextView dateOfChange;


    @Inject
    FormPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_form);
        ButterKnife.bind(this);
        ((CandidatesApplication) getApplication()).getAppComponent().inject(this);
    }

    @OnClick(R.id.confirm)
    void confirmChanges() {

        if (isFieldEmpty(nameWrapper, name)
                && isFieldEmpty(prezimeWrapper, surname)
                && isFieldEmpty(jmbgWrapper, jmbg)
                && isFieldEmpty(emailWrapper, email)
                && isFieldEmpty(cellphoneWrapper, cellphone)
        && isEmailValid(emailWrapper, email)
        && isJMBGValid(jmbgWrapper, jmbg)) {

            presenter.save(name.getText().toString(),
                    surname.getText().toString(),
                    jmbg.getText().toString(),
                    Integer.parseInt(yearOfBirth.getText().toString()),
                    email.getText().toString(),
                    cellphone.getText().toString(),
                    konkurs.getSelectedItemPosition(),
                    notes.getText().toString(),
                    isHired.isChecked(),
                    Calendar.getInstance().getTimeInMillis());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume(this);   //connect with presenter
        presenter.loadKonkursData();            //load candidates
        long id = getIntent().getLongExtra(ID, -1);

        if (id >= 0) {
            presenter.loadCandidate(id);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void presentKonkurse(ArrayList<Konkurs> results, int selectedItem) {
        konkurs.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, results));
        konkurs.setSelection(selectedItem);
    }

    @Override
    public void presentCandidate(Kandidat kandidat) {

        name.setText(kandidat.getIme());
        surname.setText(kandidat.getPrezime());
        jmbg.setText(kandidat.getJmbg());
        yearOfBirth.setText(String.valueOf(kandidat.getGodinaRodjenja()));
        email.setText(kandidat.getEmail());
        cellphone.setText(String.valueOf(kandidat.getTelefon()));
        notes.setText(kandidat.getNapomena());
        isHired.setChecked(kandidat.getDaLiJeZaposlen());

        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(kandidat.getDatumIzmene());
        dateOfChange.setText("Date changed: " + format.format(instance.getTime()));
        if (konkurs.getCount() > 0) {
            //if concurses are already loaded select right one by data in candidate
            konkurs.setSelection(presenter.getKonkursPosition(kandidat.getKonkurs()));
        }

    }

    @Override
    public void showError(String localizedMessage) {
        new AlertDialog.Builder(this)
                .setMessage(localizedMessage)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {

                })
                .setCancelable(true)
                .show();
    }

    @Override
    public void successSave() {

        Intent intent = new Intent(FormActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private boolean isFieldEmpty(TextInputLayout textInputLayout, EditText editText) {
        if (editText.getText().toString().isEmpty()) {
            textInputLayout.setError(getString(R.string.field_cant_be_empty));
            return false;
        } else {
            textInputLayout.setError(null);
            return true;
        }
    }

    private boolean isEmailValid(TextInputLayout textInputLayout, EditText email) {


        boolean isValid = EMAIL_ADDRESS_PATTERN.matcher(email.getText().toString()).matches();
        if (!isValid){
            textInputLayout.setError(getString(R.string.email_not_valid));
        } else {
            textInputLayout.setError(null);
        }

        return isValid;
    }


    private boolean isJMBGValid(TextInputLayout textInputLayout, EditText jmbg) {


        boolean isValid = jmbg.getText().toString().length() == 13;
        if (!isValid){
            textInputLayout.setError(getString(R.string.jmbg_not_valid));
        } else {
            textInputLayout.setError(null);
        }

        return isValid;
    }
}
