package com.stefana.candidates.presenters;

public abstract class Presenter<T> {


    public abstract void onResume(T view);

    public abstract void onPause();

    public abstract void onDestroy();

}
