package com.stefana.candidates.presenters;

import android.os.Build;
import android.text.TextUtils;
import android.widget.Filterable;

import com.stefana.candidates.adapters.RowAdapter;
import com.stefana.candidates.dtos.Kandidat;
import com.stefana.candidates.dtos.Konkurs;
import com.stefana.candidates.views.MainActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;


import io.realm.Realm;
import io.realm.RealmResults;

public class MainPresenter extends Presenter<MainPresenter.MainView> {

    private MainView view;
    private ArrayList<Kandidat> kandidati = new ArrayList<>();
    private Realm realm;
    private ArrayList<Konkurs> results;
    private Konkurs konkurs = new Konkurs();

    public MainPresenter(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void onResume(MainView view) {
        this.view = view;
        results = new ArrayList(realm.where(Konkurs.class).findAllAsync());

    }

    @Override
    public void onPause() {
        this.view = null;
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    public void loadData() {

        //load all data and present them sorted by surname
        try {
            kandidati = new ArrayList(realm.where(Kandidat.class)
                    .findAllAsync());

            Collections.sort(kandidati, new Comparator<Kandidat>() {
                @Override
                public int compare(Kandidat o1, Kandidat o2) {
                    return o1.getPrezime().toUpperCase().compareTo(o2.getPrezime().toUpperCase());
                }
            });

        } catch (Exception e) {
            view.showError("Error loading candidates -> " + e.getLocalizedMessage());
        }

        view.presentData(kandidati);

    }


    public void loadData(Konkurs konkurs) {
        //load data for chosed concurs
        try {
            kandidati = new ArrayList(realm.where(Kandidat.class)
                    .equalTo("konkurs.id", konkurs.getId())
                    .findAllAsync());

            view.presentData(kandidati);
        } catch (Exception e) {
            view.showError("Error loading candidates for given concurs data -> " + e.getLocalizedMessage());
        }
    }


    public void deleteData(long userID) {
        //deleting data from database
        try {
            RealmResults<Kandidat> result = realm.where(Kandidat.class).equalTo("id", userID).findAll();
            realm.beginTransaction();
            result.deleteFirstFromRealm();
            realm.commitTransaction();
            loadData();
        } catch (Exception e) {
            view.showError("Something went wrong while deleting data from database -> " + e.getLocalizedMessage());
        }
    }

    public void filterData(String newText, Konkurs selectedItem) {
        //filtering data by name, surname or JMBG
        try {
            if (selectedItem.getId() == 0) {
                //all
                kandidati = new ArrayList(realm.where(Kandidat.class)
                        .findAllAsync());
            } else {
                kandidati = new ArrayList(realm.where(Kandidat.class)
                        .equalTo("konkurs.id", selectedItem.getId())
                        .findAllAsync());
            }

            if (!TextUtils.isEmpty(newText)) {
                Iterator<Kandidat> iterator = kandidati.iterator();
                while (iterator.hasNext()) {
                    Kandidat kandidat = iterator.next();
                    if (!kandidat.toString().toLowerCase().contains(newText.toLowerCase())) {
                        iterator.remove();
                    }
                }
            }

        } catch (Exception e) {
            view.showError("Error happened while filtering data during search -> " + e.getLocalizedMessage());
        }

        view.presentData(kandidati);
    }

    public void loadKonkursData() {
        //load saved from database and present on activity
        try {
            results = new ArrayList(realm.where(Konkurs.class).findAllAsync());
            results.add(0, new Konkurs(0, "All"));

            view.initKonkursSpiner(results);
        } catch (Exception e) {
            view.showError("Error loading concurs data from database -> " + e.getLocalizedMessage());
        }
    }

    public void showRecentData() {

        //changes sorting type to present most recent changed data
        kandidati = new ArrayList(realm.where(Kandidat.class)
                .findAllAsync());

        Collections.sort(kandidati, new Comparator<Kandidat>() {
            @Override
            public int compare(Kandidat o1, Kandidat o2) {
                if (o1.getDatumIzmene() < o2.getDatumIzmene()){
                    return 1;

                } else if (o1.getDatumIzmene() > o2.getDatumIzmene()){
                    return -1;
                }
                return 0;
            }
        });

        view.presentData(kandidati);
    }

    public void insertConcurs(String ime){

        //inserting concurs on bottom navigation item (add concurs) pressed
        try {

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    konkurs.setImeKonkursa(ime);
                    if (konkurs.getId() < 0) {
                        Number currentIdNum = realm.where(Konkurs.class).max("id");

                        int nextId;

                        if (currentIdNum == null) {
                            nextId = 0;

                        } else {

                            nextId = currentIdNum.intValue() + 1;
                        }

                        konkurs.setId(nextId);
                    }

                    realm.insertOrUpdate(konkurs);

                }
            });
        } catch (Exception e){
            view.showError("Error adding new Concurs to database -> " + e.getLocalizedMessage());
        }
    }


    public interface MainView {

        void initKonkursSpiner(ArrayList<Konkurs> list);

        void presentData(ArrayList<Kandidat> data); //dummy metoda

        void showError(String localizedMessage);
    }
}
