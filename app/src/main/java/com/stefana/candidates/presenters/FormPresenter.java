package com.stefana.candidates.presenters;

import com.stefana.candidates.dtos.Kandidat;
import com.stefana.candidates.dtos.Konkurs;

import java.util.ArrayList;
import java.util.Calendar;


import io.realm.Realm;
import io.realm.RealmResults;

public class FormPresenter extends Presenter<FormPresenter.View> {

    private View view;
    private Realm realm;
    private ArrayList<Konkurs> results;
    private Kandidat kandidat = new Kandidat();

    public FormPresenter(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void onResume(View view) {
        this.view = view;
    }

    @Override
    public void onPause() {
        this.view = null;
        this.kandidat = new Kandidat();
    }

    @Override
    public void onDestroy() {
        this.view = null;
        this.kandidat = new Kandidat();

    }

    public void save(String ime, String prezime, String jmbg, int yearOfBirth, String email,
                     String cellphone, int konkursPos, String notes, boolean isHiredChecked, long time) {

        try {

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    kandidat.setIme(ime);
                    kandidat.setPrezime(prezime);
                    kandidat.setJmbg(jmbg);
                    kandidat.setGodinaRodjenja(yearOfBirth);
                    kandidat.setEmail(email);
                    kandidat.setTelefon(cellphone);
                    kandidat.setKonkurs(results.get(konkursPos));
                    kandidat.setNapomena(notes);
                    kandidat.setDaLiJeZaposlen(isHiredChecked);
                    kandidat.setDatumIzmene(time);

                    //generate new id only if it is new object (if id is -1)
                    //in every other case do update
                    if (kandidat.getId() < 0) {
                        Number currentIdNum = realm.where(Kandidat.class).max("id");

                        int nextId;

                        if (currentIdNum == null) {
                            nextId =
                                    0;

                        } else {

                            nextId = currentIdNum.intValue() + 1;
                        }

                        kandidat.setId(nextId);
                    }

                    realm.insertOrUpdate(kandidat); // using insert API
                    view.successSave();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            view.showError("Error saving changes -> " + e.getLocalizedMessage());
        }


    }

    public void loadKonkursData() {
        //load saved from database and present on activity
        try {
            results = new ArrayList(realm.where(Konkurs.class).findAllAsync());
            view.presentKonkurse(results,
                    kandidat.getKonkurs() != null ?
                            results.indexOf(kandidat.getKonkurs()) : //selektuj konkurs na formi za usera na ovoj poziciji
                            0                                       //nov user nema selekcije konkursa na formi
            );

        } catch (Exception e){
            view.showError("error loading concurs data from database -> " + e.getLocalizedMessage());

        }
    }

    public void loadCandidate(long id) {

        try {

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    kandidat = realm.where(Kandidat.class)
                            .equalTo("id", id)
                            .findFirstAsync();

                    view.presentCandidate(kandidat);
                }
            });

        } catch (Exception e) {
            view.showError("error getting chosen candidate -> " + e.getLocalizedMessage());
        }
    }

    public int getKonkursPosition(Konkurs konkurs) {
        return results.indexOf(konkurs);
    }

    public interface View {

        void presentKonkurse(ArrayList<Konkurs> results, int selectPosition);

        void presentCandidate(Kandidat kandidat);

        void showError(String localizedMessage);

        void successSave();
    }
}
